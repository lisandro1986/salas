package com.interbrain.salas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.interbrain.salas.entidades.Pelicula;

@Repository
public interface PeliculaRepository extends JpaRepository<Pelicula, String> {

    @Query("select s from Pelicula s WHERE s.baja IS NULL ORDER BY s.titulo")
    public List<Pelicula> listadoPeliculasActivas();


}
