package com.interbrain.salas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.interbrain.salas.entidades.Funcion;
import com.interbrain.salas.entidades.Reserva;

@Repository
public interface ReservaRepository extends JpaRepository<Reserva, String> {

    @Query("select s from Reserva s WHERE s.baja IS NULL")
    public List<Reserva> listadoReservasActivas();
    
    @Query("select s from Reserva s WHERE s.baja IS NULL AND s.funcion = :funcion")
    public List<Reserva> listadoReservasPorFuncion(@Param("funcion") Funcion funcion);


}
