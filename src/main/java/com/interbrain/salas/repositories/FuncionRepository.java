package com.interbrain.salas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.interbrain.salas.entidades.Funcion;

@Repository
public interface FuncionRepository extends JpaRepository<Funcion, String> {

    @Query("select s from Funcion s WHERE s.baja IS NULL ORDER BY s.pelicula.titulo")
    public List<Funcion> listadoFuncionesActivas();


}
