package com.interbrain.salas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.interbrain.salas.entidades.Sala;

@Repository
public interface SalaRepository extends JpaRepository<Sala, String> {

    @Query("select s from Sala s WHERE s.baja IS NULL ORDER BY s.nombre")
    public List<Sala> listadoSalasActivas();


}
