package com.interbrain.salas.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
public class Reserva implements Serializable {

	private static final long serialVersionUID = -1340013368443652753L;

	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Id
	private String id;

	@ManyToOne
	private Funcion funcion;

	@Email(message = "Por favor revise su direccion de email")
	private String email;
	private String nombre;
	private String apellido;
	private String telefono;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date alta;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date baja;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificacion;

}
