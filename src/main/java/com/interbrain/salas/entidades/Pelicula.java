package com.interbrain.salas.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.interbrain.salas.enumeraciones.Generos;

import lombok.Data;

@Data
@Entity
public class Pelicula implements Serializable{
	
	private static final long serialVersionUID = -1340013368443652753L;

	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Id
	private String id;
	
	private String titulo;
	private String protagonistas;
	
	@Enumerated(EnumType.STRING)
	private Generos genero;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date alta;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date baja;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificacion;

}
