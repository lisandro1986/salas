package com.interbrain.salas.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interbrain.salas.entidades.Funcion;
import com.interbrain.salas.repositories.FuncionRepository;

@Service("funcionService")
public class FuncionService {

	@Autowired
	private FuncionRepository funcionRepository;

	public List<Funcion> listadoFunciones() {

		return funcionRepository.listadoFuncionesActivas();
	}

	public boolean tieneId(Funcion funcion) {
		if (funcion.getId() != null) {
			return true;
		} else {
			return false;
		}
	}

	public void eliminarFuncion(String id) {
		Funcion funcion = buscar(id);
		funcion.setBaja(new Date());
		funcionRepository.save(funcion);
	}

	public Funcion buscar(String id) {
		Optional<Funcion> resultado = funcionRepository.findById(id);
		if (resultado.isPresent()) {
			return resultado.get();
		}
		return null;
	}

	public void guardarFuncion(Funcion funcion) {

		if (tieneId(funcion)) {
			funcion.setModificacion(new Date());
			Funcion funcionOriginal = buscar(funcion.getId());
			funcion.setAlta(funcionOriginal.getAlta());
		} else {
			funcion.setAlta(new Date());

		}
		funcionRepository.save(funcion);
	}

}
