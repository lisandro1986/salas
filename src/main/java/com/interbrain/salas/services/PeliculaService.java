package com.interbrain.salas.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interbrain.salas.entidades.Pelicula;
import com.interbrain.salas.repositories.PeliculaRepository;

@Service("peliculaService")
public class PeliculaService {

	@Autowired
	private PeliculaRepository peliculaRepository;

	public List<Pelicula> listadoPeliculas() {

		return peliculaRepository.listadoPeliculasActivas();
	}

	public boolean tieneId(Pelicula pelicula) {
		if (pelicula.getId() != null) {
			return true;
		} else {
			return false;
		}
	}

	public void eliminarPelicula(String id) {
		Pelicula pelicula = buscar(id);
		pelicula.setBaja(new Date());
		peliculaRepository.save(pelicula);
	}

	public Pelicula buscar(String id) {
		Optional<Pelicula> resultado = peliculaRepository.findById(id);
		if (resultado.isPresent()) {
			return resultado.get();
		}
		return null;
	}

	public void guardarPelicula(Pelicula pelicula) {

		if (tieneId(pelicula)) {
			pelicula.setModificacion(new Date());
			Pelicula peliculaOriginal = buscar(pelicula.getId());
			pelicula.setAlta(peliculaOriginal.getAlta());
		} else {
			pelicula.setAlta(new Date());

		}
		peliculaRepository.save(pelicula);
	}

}
