package com.interbrain.salas.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interbrain.salas.entidades.Funcion;
import com.interbrain.salas.entidades.Reserva;
import com.interbrain.salas.repositories.ReservaRepository;

@Service("reesrvaService")
public class ReservaService {

	@Autowired
	private ReservaRepository reservaRepository;

	public List<Reserva> listadoReservas() {

		return reservaRepository.listadoReservasActivas();
	}

	public boolean tieneId(Reserva reserva) {
		if (reserva.getId() != null) {
			return true;
		} else {
			return false;
		}
	}

	public void eliminarReserva(String id) {
		Reserva reserva = buscar(id);
		reserva.setBaja(new Date());
		reservaRepository.save(reserva);
	}

	public Reserva buscar(String id) {
		Optional<Reserva> resultado = reservaRepository.findById(id);
		if (resultado.isPresent()) {
			return resultado.get();
		}
		return null;
	}

	public void guardarReserva(Reserva reserva) {

		if (tieneId(reserva)) {
			reserva.setModificacion(new Date());
			Reserva reservaOriginal = buscar(reserva.getId());
			reserva.setAlta(reservaOriginal.getAlta());
		} else {
			reserva.setAlta(new Date());

		}
		reservaRepository.save(reserva);
	}

	public int asientosDisponibles(Funcion funcion) {
		List<Reserva> reservas = reservaRepository.listadoReservasPorFuncion(funcion);
		int asientosDisponibles = funcion.getSala().getCapacidad() - reservas.size();
		return asientosDisponibles;
	}

	public List<Funcion> funcionesConAsientosDisponibles(List<Funcion> funciones) {
		for (Funcion funcion : funciones) {
			funcion.setAsientosDisponibles(asientosDisponibles(funcion));
		}
		return funciones;
	}

	public List<Reserva> reservasPorFuncion(Funcion funcion) {

		return reservaRepository.listadoReservasPorFuncion(funcion);
	}

}
