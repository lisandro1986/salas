package com.interbrain.salas.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interbrain.salas.entidades.Sala;
import com.interbrain.salas.repositories.SalaRepository;

@Service("salaService")
public class SalaService {

	@Autowired
	private SalaRepository salaRepository;

	public List<Sala> listadoSalas() {

		return salaRepository.listadoSalasActivas();
	}

	public boolean tieneId(Sala pelicula) {
		if (pelicula.getId() != null) {
			return true;
		} else {
			return false;
		}
	}

	public void eliminarSala(String id) {
		Sala pelicula = buscar(id);
		pelicula.setBaja(new Date());
		salaRepository.save(pelicula);
	}

	public Sala buscar(String id) {
		Optional<Sala> resultado = salaRepository.findById(id);
		if (resultado.isPresent()) {
			return resultado.get();
		}
		return null;
	}

	public void guardarSala(Sala sala) {

		if (tieneId(sala)) {
			sala.setModificacion(new Date());
			Sala salaOriginal = buscar(sala.getId());
			sala.setAlta(salaOriginal.getAlta());
		} else {
			sala.setAlta(new Date());

		}
		salaRepository.save(sala);
	}

}
