package com.interbrain.salas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.interbrain.salas.entidades.Sala;
import com.interbrain.salas.services.SalaService;
import com.interbrain.salas.utilidades.Vistas;

@Controller
@RequestMapping("/sala")
public class SalaController {
	
	@Autowired
	private SalaService salaService;

	@GetMapping("/listado")
	public String inicio(Model modelo) {
		
		modelo.addAttribute("salas", salaService.listadoSalas());
		
		return Vistas.SALAS_LISTADO;
		
	}
	@GetMapping("/crear")
	public String crearSala(Model modelo) {
		
		modelo.addAttribute("sala", new Sala());

		return Vistas.SALA_EDICION;
	}

	@GetMapping("/editar/{id}")
	public String editarSala(@PathVariable(name = "id") String id, Model modelo) {

		Sala sala = salaService.buscar(id);

		modelo.addAttribute("sala", sala);

		return Vistas.SALA_EDICION;
	}

	@PostMapping("/guardar")
	public String guardarEdicion(@ModelAttribute("sala") Sala sala) {

		salaService.guardarSala(sala);

		return "redirect:/sala/listado";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminarSala(@PathVariable(name = "id") String id) {

		salaService.eliminarSala(id);

		return "redirect:/sala/listado";
	}
}
