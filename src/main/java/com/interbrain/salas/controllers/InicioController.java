package com.interbrain.salas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.interbrain.salas.entidades.Funcion;
import com.interbrain.salas.services.FuncionService;
import com.interbrain.salas.services.ReservaService;
import com.interbrain.salas.utilidades.Vistas;

@Controller
public class InicioController {
	
	@Autowired
	private FuncionService funcionService;
	
	@Autowired
	private ReservaService reservaService;

	@GetMapping("")
	public String inicio(Model modelo) {
		
		List<Funcion> funciones = funcionService.listadoFunciones();
		funciones = reservaService.funcionesConAsientosDisponibles(funciones);
		
		modelo.addAttribute("funciones", funciones);
		
		return Vistas.INICIO;
		
	}
	
}
