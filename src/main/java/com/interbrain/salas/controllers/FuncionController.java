package com.interbrain.salas.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.interbrain.salas.entidades.Funcion;
import com.interbrain.salas.services.FuncionService;
import com.interbrain.salas.services.PeliculaService;
import com.interbrain.salas.services.SalaService;
import com.interbrain.salas.utilidades.Vistas;

@Controller
@RequestMapping("/funcion")
public class FuncionController {

	@Autowired
	private FuncionService funcionService;

	@Autowired
	private PeliculaService peliculaService;

	@Autowired
	private SalaService salaService;

	@GetMapping("/listado")
	public String inicio(Model modelo) {

		modelo.addAttribute("funciones", funcionService.listadoFunciones());

		return Vistas.FUNCIONES_LISTADO;

	}

	@GetMapping("/crear")
	public String crearFuncion(Model modelo) {

		modelo.addAttribute("funcion", new Funcion());
		modelo.addAttribute("peliculas", peliculaService.listadoPeliculas());
		modelo.addAttribute("salas", salaService.listadoSalas());

		return Vistas.FUNCION_EDICION;
	}

	@GetMapping("/editar/{id}")
	public String editarFuncion(@PathVariable(name = "id") String id, Model modelo) {

		Funcion funcion = funcionService.buscar(id);

		modelo.addAttribute("funcion", funcion);
		modelo.addAttribute("peliculas", peliculaService.listadoPeliculas());
		modelo.addAttribute("salas", salaService.listadoSalas());

		return Vistas.FUNCION_EDICION;
	}

	@PostMapping("/guardar")
	public String guardarEdicion(@ModelAttribute("funcion") Funcion funcion,
			@RequestParam(required = false) String peliculaId, @RequestParam(required = false) String salaId, @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaSeparada) {

		if (peliculaId != null) {
			funcion.setPelicula(peliculaService.buscar(peliculaId));
		}

		if (salaId != null) {
			funcion.setSala(salaService.buscar(salaId));
		}
		
		if (fechaSeparada != null) {
			funcion.setFecha(fechaSeparada);
		}
		funcionService.guardarFuncion(funcion);

		return "redirect:/funcion/listado";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminarFuncion(@PathVariable(name = "id") String id) {

		funcionService.eliminarFuncion(id);

		return "redirect:/funcion/listado";
	}
}
