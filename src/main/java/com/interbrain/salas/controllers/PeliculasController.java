package com.interbrain.salas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.interbrain.salas.entidades.Pelicula;
import com.interbrain.salas.services.PeliculaService;
import com.interbrain.salas.utilidades.Vistas;

@Controller
@RequestMapping("/pelicula")
public class PeliculasController {
	
	@Autowired
	private PeliculaService peliculaService;

	@GetMapping("/listado")
	public String inicio(Model modelo) {
		
		modelo.addAttribute("peliculas", peliculaService.listadoPeliculas());
		
		return Vistas.PELICULAS_LISTADO;
		
	}
	@GetMapping("/crear")
	public String crearPelicula(Model modelo) {
		
		modelo.addAttribute("pelicula", new Pelicula());

		return Vistas.PELICULA_EDICION;
	}

	@GetMapping("/editar/{id}")
	public String editarProducto(@PathVariable(name = "id") String id, Model modelo) {

		Pelicula pelicula = peliculaService.buscar(id);

		modelo.addAttribute("pelicula", pelicula);

		return Vistas.PELICULA_EDICION;
	}

	@PostMapping("/guardar")
	public String guardarEdicion(@ModelAttribute("pelicula") Pelicula pelicula) {

		peliculaService.guardarPelicula(pelicula);

		return "redirect:/pelicula/listado";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminarPelicula(@PathVariable(name = "id") String id) {

		peliculaService.eliminarPelicula(id);

		return "redirect:/pelicula/listado";
	}
}
