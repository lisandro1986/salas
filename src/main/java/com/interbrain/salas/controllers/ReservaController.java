package com.interbrain.salas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.interbrain.salas.entidades.Reserva;
import com.interbrain.salas.services.FuncionService;
import com.interbrain.salas.services.ReservaService;
import com.interbrain.salas.utilidades.Vistas;

@Controller
@RequestMapping("/reserva")
public class ReservaController {

	@Autowired
	private ReservaService reservaService;

	@Autowired
	private FuncionService funcionService;

	@GetMapping("/listado/{id}")
	public String inicio(@PathVariable(name = "id") String idFuncion, Model modelo) {

		modelo.addAttribute("reservas", reservaService.reservasPorFuncion(funcionService.buscar(idFuncion)));
		modelo.addAttribute("funcion", funcionService.buscar(idFuncion));

		return Vistas.RESERVAS_LISTADO;

	}

	@GetMapping("/crear/{id}")
	public String crearReserva(@PathVariable(name = "id") String idFuncion, Model modelo) {

		modelo.addAttribute("reserva", new Reserva());
		modelo.addAttribute("funcion", funcionService.buscar(idFuncion));

		return Vistas.RESERVA_EDICION;
	}

	@GetMapping("/editar/{id}")
	public String editarReserva(@PathVariable(name = "id") String id, Model modelo) {

		Reserva reserva = reservaService.buscar(id);

		modelo.addAttribute("reserva", reserva);
		modelo.addAttribute("funcion", reserva.getFuncion());


		return Vistas.RESERVA_EDICION;
	}

	@PostMapping("/guardar")
	public String guardarEdicion(@ModelAttribute("reserva") Reserva reserva,
			@RequestParam(required = true) String idFuncion) {

		if (idFuncion != null) {
			reserva.setFuncion(funcionService.buscar(idFuncion));
		}
		reservaService.guardarReserva(reserva);

		return "redirect:/reserva/listado/" + reserva.getFuncion().getId();

	}

	@GetMapping("/eliminar/{id}")
	public String eliminarReserva(@PathVariable(name = "id") String id) {
		
		Reserva reserva = reservaService.buscar(id);
		String funcionId = reserva.getFuncion().getId();

		reservaService.eliminarReserva(id);

		return "redirect:/reserva/listado/" + funcionId;
	}
}
