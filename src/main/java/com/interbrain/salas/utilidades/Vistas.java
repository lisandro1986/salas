package com.interbrain.salas.utilidades;

public final class Vistas {

	public static final String INICIO = "index.html";
	public static final String PELICULAS_LISTADO = "peliculas.html";
	public static final String PELICULA_EDICION = "pelicula-edicion.html";
	public static final String SALAS_LISTADO = "salas.html";
	public static final String SALA_EDICION = "sala-edicion.html";
	public static final String FUNCIONES_LISTADO = "funciones.html";
	public static final String FUNCION_EDICION = "funcion-edicion.html";
	public static final String RESERVAS_LISTADO = "reservas.html";
	public static final String RESERVA_EDICION = "reserva-edicion.html";

	private Vistas() {
	}
}
