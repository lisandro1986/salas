# Salas

Demo para reserva de salas de cine.

EL back-end del sistema esta desarrollado con el framework Spring del lenguaje Java, integrando JPA y MySQL como motor de base de datos para almacenar la informacion.

El sistema esta desarrollado bajo el paradigma de orientacion a objetos.

Para ejecutarlo es necesario modificar el archivo application.properties ingresando los datos de conexion y nombre de base de datos.

El front-end esta desarrollado en HTML + CSS + Bootstrap.

Para su funcionamiento deben cargarse a la base de datos:

1- Peliculas
2- Salas
3- Funciones que incluyan estas peliculas y salas

Luego podran registrarse las reservas para estas funciones.

Sistema desarrollado por Lisandro Scofano.
lisandroscofano@gmail.com